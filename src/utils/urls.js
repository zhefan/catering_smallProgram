export default {
    /**@name  菜品 */
    GET_listAll: '/mobile/dishes/listAll',
    /**@name 订单 */
    orderBook: '/mobile/order/book',
    /**@name 列表 */
    List: '/mobile/dishes/list',
    /**@name 微信登录 */
    wxAuth: '/mobile/wx/h5auth',
    /**@name 获取用户信息 */
    getUser: '/mobile/wx/getUser',
    /**@name 下单成功调微信下单 */
    UNIFIED_ORDER: '/mobile/wx/unifiedOrder',
    /**@name 订单列表 */
    listAll: '/mobile/order/listAll'
}