import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import states from '@/store/computed'
import MintUI from 'mint-ui'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import Vant from 'vant'
import api from './utils/request'
import 'vant/lib/index.css'
import 'swiper/dist/css/swiper.css'
import 'mint-ui/lib/style.css'
import './assets/css/main.less'
import * as filters from './filters'
import technical from '@/commom/technical/index.js'
Vue.use(technical)
// 全局混入
Vue.mixin({
    computed: states
})
Vue.config.productionTip = false
Vue.use(Vant)
Vue.use(VueAwesomeSwiper /* { default global options } */)
// import { Toast } from 'vant';

// Vue.use(Toast);
Vue.prototype.$api = api //全局定义api请求的方法
Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
})
Vue.use(MintUI)
// import Vconsole from 'vconsole/dist/vconsole.min.js'
// /* eslint-disable */

// let vConsole = new Vconsole()
/* eslint-disable */
document.getElementsByTagName('html')[0].style.fontSize =
    document.getElementsByTagName('html')[0].offsetWidth / 7.5 + 'px'
window.onresize = function () {
    document.getElementsByTagName('html')[0].style.fontSize =
        document.getElementsByTagName('html')[0].offsetWidth / 7.5 + 'px'
}
// import Vconsole from 'vconsole/dist/vconsole.min.js'
// /* eslint-disable */

// let vConsole = new Vconsole()
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')