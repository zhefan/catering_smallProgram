import URL from './urls'
import $http from './http'
export default {
    /**@name 菜品列表 All All */
    dishesListAll(params) {
        return $http.http('get', URL.GET_listAll, params)
    },
    /**@name 下单 */
    orderBook(params) {
        return $http.http('post', URL.orderBook, params)
    },
    /**@name 列表 */
    List(params) {
        return $http.http('get', URL.List, params)
    },
    /**@name 微信登录 */
    wxAuth(params) {
        return $http.http('get', URL.wxAuth, params)
    },
    /**@name 获取用户信息 */
    getUser(params) {
        return $http.http('post', URL.getUser, params)
    },
    /**@name 下单成功调微信下单 */
    unifiedOrder(params) {
        return $http.http('post', URL.UNIFIED_ORDER, params)
    },
    /**@name 订单列表 */
    listAll(params) {
        return $http.http('get', URL.listAll, params)
    }
}