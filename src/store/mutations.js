export default {
    ChangeOrder(state, data) {
        state.orderData = data
    },
    ChangeUser(state, data) {
        state.userInfo = data
    },
    requestLoading(state, Bool) {
        state.loadingLock = Bool
    },
    //改变购物车 加减
    changeSelect(state, obj) {
        //去重
        let Selectgoods = JSON.parse(JSON.stringify(state.Selectgoods))

        function preparation(arr, item) {
            for(var i = 0; i < arr.length; i++) {
                if(arr[i].id == item.id) {
                    return i
                }
            }
            return -1
        }
        var index = preparation(Selectgoods, obj.newfood)
        if(index != -1) {
            Selectgoods[index].num = obj.val
        } else if(obj.val != 0) {
            Selectgoods.push(obj.newfood)
        }
        if(obj.val == 0) {
            let spliceIndex = 0
            for(let j = 0; j < Selectgoods.length; j++) {
                let item = Selectgoods[j]
                if(item.id == obj.newfood.id) {
                    spliceIndex = j
                }
            }
            Selectgoods.splice(Selectgoods.indexOf(Selectgoods[spliceIndex]), 1)
        }
        state.Selectgoods = Selectgoods
    },
    emptySelect(state) {
        state.Selectgoods = []
    }
}