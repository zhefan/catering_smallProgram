import {
    mapState
} from 'vuex'

export default mapState([
    'shopId', //店铺id
    'tableName', //桌子id
    'orderData', //订单信息
    'userInfo', //用户信息
    'Selectgoods', //选中的菜
    'loadingLock' //请求加锁
])