'use strict'
import axios from 'axios'
import Vue from 'vue'
import store from '@/store'
// axios 配置
// 配置API接口地址
// let root = '47.107.248.96'   线上ip
// let root = '192.168.11.41'   本地ip
// 192.168.11.244
// let root = 'http://192.168.11.41:8080'
// let root = 'http://huzhenquan.com:8080' ///192.168.11.244这个ip才要端口
let root = 'http://www.huzhenquan.com'
    // axios.defaults.baseURL = root
axios.defaults.baseURL = root
    // 响应时间
axios.defaults.timeout = 10000
    // `withCredentails`选项表明了是否是跨域请求
axios.defaults.withCredentials = true
Vue.prototype.$baseUrl = axios.defaults.baseURL
    // 设置默认请求头
axios.defaults.headers = {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
    'Access-Control-Allow-Credentials': 'true'
}
Vue.prototype.$baseUrl = axios.defaults.baseURL
    // 添加请求拦截器
axios.interceptors.request.use(
        config => {
            store.commit('requestLoading', true) //统一加锁
            return config
        },
        error => {
            store.commit('requestLoading', true) //统一加锁
            return Promise.reject(error)
        }
    )
    // 响应时拦截
axios.interceptors.response.use(
    function(response) {
        // if()
        store.commit('requestLoading', false) //统一开锁
        if (response.data.code === 1020) {
            window.location.href = `${root}/mobile/wx/h5auth?url=${window.location.href}`
        } else {
            return response.data
        }

    },
    function(error) {
        store.commit('requestLoading', false) //统一开锁
        return Promise.reject(error.response.data)
    }
)

export default {
    http(method, url, params) {
        return axios({
                method,
                url,
                params: method === 'get' && params,
                data: method !== 'get' && params
            })
            .then(res => {
                return res
            })
            .catch(err => {
                return err
            })
    },
    all(fn1, fn2) {
        return axios
            .all([fn1(), fn2()])
            .then(
                axios.spread(function(acct, perms) {
                    return [acct, perms]
                })
            )
            .catch(err => {
                return err
            })
    }
}